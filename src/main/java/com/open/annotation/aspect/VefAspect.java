package com.open.annotation.aspect;

import com.open.bean.vo.OpenVo;
import com.open.exception.WebException;
import com.open.util.OpenRequestUtil;
import com.open.util.RequestUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@Aspect
@Component
public class VefAspect {


    @Before(value = "@annotation(com.open.annotation.Vef)")
    public void doBefore(JoinPoint joinPoint) throws WebException {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attributes != null){
            HttpServletRequest request = attributes.getRequest();
            OpenVo openVo = OpenRequestUtil.vefUser(request);
            if (!openVo.isState()) {
                throw new WebException(openVo.getMsg());
            }
        }
    }

}

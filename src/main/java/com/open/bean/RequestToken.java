package com.open.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName request_token
 */
@TableName(value ="request_token")
@Data
public class RequestToken implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer tid;

    /**
     * 密钥
     */
    private String sign;

    /**
     * 0=正常，1=失效
     */
    private Integer signStatus;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 余额使用
     */
    private Double balance;

    /**
     * 0=公众号token,1=图片token
     */
    private Integer tokenType;

    /**
     * 备注
     */
    private String remark;

    /**
     * 版本
     */
    private String version;

    /**
     * 主密钥
     */
    private String mainSign;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
package com.open.controller;

import com.open.bean.FunctionUpdate;
import com.open.result.OpenResult;
import com.open.service.FunctionUpdateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author typsusan
 * <p>des</p>
 **/
@RestController
@RequestMapping("/request/open")
@Slf4j
public class FunctionUpdateController {

    @Autowired
    FunctionUpdateService functionUpdateService;

    @GetMapping("/function/update/list")
    public OpenResult list(){
        List<FunctionUpdate> functionUpdateList = functionUpdateService.lambdaQuery().orderByDesc(FunctionUpdate::getFid).list();
        return OpenResult.success(functionUpdateList);
    }

}

package com.open.mapper;

import com.open.bean.OpenSystemRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【open_system_role】的数据库操作Mapper
* @createDate 2023-04-26 11:20:23
* @Entity com.open.bean.OpenSystemRole
*/
public interface OpenSystemRoleMapper extends BaseMapper<OpenSystemRole> {

}





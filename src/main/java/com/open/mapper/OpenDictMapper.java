package com.open.mapper;

import com.open.bean.OpenDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.open.bean.OpenSystemRole;

import java.util.List;

/**
* @author typsusan
* @description 针对表【open_dict】的数据库操作Mapper
* @createDate 2023-03-19 00:34:13
* @Entity com.open.bean.OpenDict
*/
public interface OpenDictMapper extends BaseMapper<OpenDict> {

    List<OpenSystemRole> listByRole();
}





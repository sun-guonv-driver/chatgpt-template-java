package com.open.mapper;

import com.open.bean.IpWhite;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【ip_white】的数据库操作Mapper
* @createDate 2023-07-26 10:54:39
* @Entity com.open.bean.IpWhite
*/
public interface IpWhiteMapper extends BaseMapper<IpWhite> {

}





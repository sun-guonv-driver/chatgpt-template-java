package com.open.mapper;

import com.open.bean.FunctionUpdate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【function_update(功能点更新表)】的数据库操作Mapper
* @createDate 2023-07-26 16:41:47
* @Entity com.open.bean.FunctionUpdate
*/
public interface FunctionUpdateMapper extends BaseMapper<FunctionUpdate> {

}





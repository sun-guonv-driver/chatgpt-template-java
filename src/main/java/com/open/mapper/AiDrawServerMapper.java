package com.open.mapper;

import com.open.bean.AiDrawServer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author typsusan
* @description 针对表【ai_draw_server】的数据库操作Mapper
* @createDate 2023-05-26 22:45:22
* @Entity com.open.bean.AiDrawServer
*/
public interface AiDrawServerMapper extends BaseMapper<AiDrawServer> {

    List<AiDrawServer> getAiDrawServer(@Param("wid") String wid);
}





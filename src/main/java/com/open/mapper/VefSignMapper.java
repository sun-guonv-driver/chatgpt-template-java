package com.open.mapper;

import com.open.bean.VefSign;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author typsusan
* @description 针对表【vef_sign】的数据库操作Mapper
* @createDate 2023-03-18 20:37:10
* @Entity com.open.bean.VefSign
*/
public interface VefSignMapper extends BaseMapper<VefSign> {

}





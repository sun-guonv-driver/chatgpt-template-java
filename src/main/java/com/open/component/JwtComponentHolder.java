package com.open.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@Component
public class JwtComponentHolder {
    private static JwtComponent jwtComponent;

    @Autowired
    public void setJwtComponent(JwtComponent jwtComponent) {
        JwtComponentHolder.jwtComponent = jwtComponent;
    }

    public static JwtComponent getInstance() {
        return jwtComponent;
    }
}

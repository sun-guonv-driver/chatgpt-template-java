package com.open.service;

import com.open.bean.DistributionRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【distribution_record】的数据库操作Service
* @createDate 2023-06-29 16:14:23
*/
public interface DistributionRecordService extends IService<DistributionRecord> {

}

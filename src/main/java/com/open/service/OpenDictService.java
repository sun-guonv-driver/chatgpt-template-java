package com.open.service;

import com.open.bean.OpenDict;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【open_dict】的数据库操作Service
* @createDate 2023-03-19 00:34:13
*/
public interface OpenDictService extends IService<OpenDict> {

    String getDict(String code);

    void setDict(String code,Object obj);

    void subtractDict(String code,Object obj);

}

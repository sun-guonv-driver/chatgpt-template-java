package com.open.service;

import com.open.bean.IpWhite;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【ip_white】的数据库操作Service
* @createDate 2023-07-26 10:54:39
*/
public interface IpWhiteService extends IService<IpWhite> {

}

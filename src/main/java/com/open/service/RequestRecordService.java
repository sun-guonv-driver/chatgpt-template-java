package com.open.service;

import com.open.bean.RequestRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【request_record】的数据库操作Service
* @createDate 2023-06-30 11:02:07
*/
public interface RequestRecordService extends IService<RequestRecord> {

}

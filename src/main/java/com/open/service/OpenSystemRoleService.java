package com.open.service;

import com.open.bean.OpenSystemRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author typsusan
* @description 针对表【open_system_role】的数据库操作Service
* @createDate 2023-04-26 11:20:23
*/
public interface OpenSystemRoleService extends IService<OpenSystemRole> {

    List<OpenSystemRole> listByRole();

}

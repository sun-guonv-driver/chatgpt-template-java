package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.OpenSystemRole;
import com.open.mapper.OpenDictMapper;
import com.open.service.OpenSystemRoleService;
import com.open.mapper.OpenSystemRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author typsusan
* @description 针对表【open_system_role】的数据库操作Service实现
* @createDate 2023-04-26 11:20:23
*/
@Service
public class OpenSystemRoleServiceImpl extends ServiceImpl<OpenSystemRoleMapper, OpenSystemRole>
    implements OpenSystemRoleService{

    @Autowired
    OpenDictMapper openDictMapper;

    @Override
    public List<OpenSystemRole> listByRole() {
        return openDictMapper.listByRole();
    }
}





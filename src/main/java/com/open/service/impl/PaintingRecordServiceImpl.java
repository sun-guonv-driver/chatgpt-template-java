package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.PaintingRecord;
import com.open.bean.vo.PaintingRecordVo;
import com.open.service.PaintingRecordService;
import com.open.mapper.PaintingRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author typsusan
* @description 针对表【painting_record】的数据库操作Service实现
* @createDate 2023-05-27 11:43:03
*/
@Service
public class PaintingRecordServiceImpl extends ServiceImpl<PaintingRecordMapper, PaintingRecord>
    implements PaintingRecordService{

    @Autowired
    PaintingRecordMapper paintingRecordMapper;

    @Override
    public List<Integer> getServerCount(Integer sid) {
        return paintingRecordMapper.getServerCount(sid);
    }

    @Override
    public List<PaintingRecord> getPaintingList(Integer sid, Integer serverType, Integer pageSize, Integer currentPage) {
        int startIndex = (currentPage - 1) * pageSize;
        return paintingRecordMapper.getPaintingList(sid,serverType,pageSize,startIndex);
    }

    @Override
    public List<PaintingRecordVo> getAllImg() {
        return paintingRecordMapper.getAllImg();
    }

    @Override
    public List<PaintingRecordVo> getToImg(String wid) {
        return paintingRecordMapper.getToImg(wid);
    }

    @Override
    public List<PaintingRecord> byDays() {
        return paintingRecordMapper.byDays();
    }
}





package com.open.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.open.bean.LoginRecord;
import com.open.service.LoginRecordService;
import com.open.mapper.LoginRecordMapper;
import org.springframework.stereotype.Service;

/**
* @author typsusan
* @description 针对表【login_record(登录记录)】的数据库操作Service实现
* @createDate 2023-05-06 15:18:23
*/
@Service
public class LoginRecordServiceImpl extends ServiceImpl<LoginRecordMapper, LoginRecord>
    implements LoginRecordService{

}





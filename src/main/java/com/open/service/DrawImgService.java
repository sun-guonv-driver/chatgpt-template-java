package com.open.service;

import com.open.bean.DrawImg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author typsusan
* @description 针对表【draw_img】的数据库操作Service
* @createDate 2023-05-18 18:04:58
*/
public interface DrawImgService extends IService<DrawImg> {

}

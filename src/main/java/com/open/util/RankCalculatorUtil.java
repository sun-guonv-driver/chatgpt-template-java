package com.open.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
public class RankCalculatorUtil {

    public static String calculateRank(int points) {
        if (points >=0 && points < 10){
            return "菜鸟出道";
        }else if (points >=10 && points< 30 ){
            return "绘画能手";
        }else if (points >=30 && points<100){
            return "绘画高手";
        }else if (points >=100 && points< 500){
            return "绘画伯爵";
        }else if (points >=500 && points< 1000){
            return "神笔马良";
        }else if (points >=1000 && points< 5000){
            return "绘画砖石";
        }else if (points >=5000 && points< 10000){
            return "绘画大师";
        }else {
            return "绘画王者";
        }
    }

}

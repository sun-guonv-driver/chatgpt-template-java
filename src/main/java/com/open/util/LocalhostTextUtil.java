package com.open.util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author typsusan
 * <p>des</p>
 **/
public class LocalhostTextUtil {

    public static List<String> text(){
        List<String> textTest = new ArrayList<>();
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"这\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"是\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"一\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"条\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"测\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"试\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"消\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"息\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"，\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"专\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"门\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"针\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"对\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"本\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"地\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"Test\"},\"finish_reason\":null}]}");

        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"，\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"可\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"将\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"当\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"前\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"项\"},\"finish_reason\":null}]}");

        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"目\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"部\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"署\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"到\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"服\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"务\"},\"finish_reason\":null}]}");


        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"器\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"后\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"，将\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"request_token\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"表\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"token_type\"},\"finish_reason\":null}]}");


        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"等于6\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"设置\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"，你\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"新\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"的\"},\"finish_reason\":null}]}");
        textTest.add("data: {\"id\":\"chatcmpl-7o0LduWTiJPJahMJa0Ok3G2gx5NYB\",\"object\":\"chat.completion.chunk\",\"created\":1692152093,\"model\":\"gpt-3.5-turbo-0613\",\"choices\":[{\"index\":0,\"delta\":{\"content\":\"key\"},\"finish_reason\":null}]}");


        textTest.add("data: [DONE]");
        return textTest;
    }
}

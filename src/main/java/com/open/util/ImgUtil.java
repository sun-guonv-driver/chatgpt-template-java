package com.open.util;

import cn.hutool.core.io.FileUtil;
import org.apache.commons.codec.binary.Base64;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
public class ImgUtil {

    public static final String AI_IMG_PATH = "/data/chat/";

    public static final String TEXT_IMG_PATH = "/data/chat/identify/";

    public static String generateImage(String base64,String path,String suffix) {
        try {
            String imgClassPath = path.concat(java.util.UUID.randomUUID().toString()).concat(suffix);
            byte[] bytes = Base64.decodeBase64(base64.getBytes());
            OutputStream out = new FileOutputStream(imgClassPath);
            out.write(bytes);
            out.flush();
            out.close();
            // 返回图片的相对路径 = 图片分类路径+图片名+图片后缀
            return imgClassPath.replace("/data","");
        } catch (IOException e) {
            return null;
        }
    }

}

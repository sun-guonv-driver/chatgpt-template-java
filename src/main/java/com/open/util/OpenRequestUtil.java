package com.open.util;

import cn.hutool.core.util.StrUtil;
import com.open.bean.vo.OpenVo;
import com.open.bean.VefSign;
import com.open.service.impl.VefSignServiceImpl;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author page-view
 * <p>des</p>
 **/
@Slf4j
public class OpenRequestUtil {

    public static final String USER_AGENT = "User-Agent";

    public static final String REQUEST_HEADER_SING = "Accept-page-code";

    public static OpenVo vefUser(HttpServletRequest request){
        OpenVo openVo = new OpenVo(true,"成功");
        String header = request.getHeader(USER_AGENT);
        try {
            if (StrUtil.isBlank(header)){
                openVo.setState(false);
                openVo.setMsg("请求出现一个未知的错误 ERROR.M1M");
                return openVo;
            }

            String signCode = request.getHeader(REQUEST_HEADER_SING);
            if (StrUtil.isBlank(signCode)){
                openVo.setState(false);
                openVo.setMsg("请求出现一个未知的错误 ERROR.M5M");
                return openVo;
            }else {
                VefSignServiceImpl signService = InterceptorBeanUtil.getBean(VefSignServiceImpl.class, request);
                if (signService.lambdaQuery().eq(VefSign::getSign,signCode).count() == 0) {
                    if (!DESUtil.decryptAcceptPageCode(signCode)){
                        openVo.setState(false);
                        openVo.setMsg("请求出现一个未知的错误 ERROR.M6M");
                        return openVo;
                    }else {
                        signService.save(new VefSign(null,signCode,new Date()));
                    }
                }else {
                    openVo.setState(false);
                    openVo.setMsg("重复提交请求，请稍后再试");
                    return openVo;
                }
            }
            return openVo;
        }catch (Exception e){
            openVo.setState(false);
            openVo.setMsg("请求异常 解析请求失败");
            return openVo;
        }
    }

    public static OpenVo vefUser(HttpServletRequest request,String currentHeader){
        OpenVo openVo = new OpenVo(true,"成功");
        String header = request.getHeader(USER_AGENT);
        try {
            if (StrUtil.isBlank(header)){
                openVo.setState(false);
                openVo.setMsg("请求出现一个未知的错误 ERROR.M1M");
                return openVo;
            }
            if (StrUtil.isBlank(currentHeader)){
                openVo.setState(false);
                openVo.setMsg("请求出现一个未知的错误 ERROR.M5M");
                return openVo;
            }else {
                VefSignServiceImpl signService = InterceptorBeanUtil.getBean(VefSignServiceImpl.class, request);
                if (signService.lambdaQuery().eq(VefSign::getSign,currentHeader).count() == 0) {
                    if (!DESUtil.decryptAcceptPageCode(currentHeader)){
                        openVo.setState(false);
                        openVo.setMsg("请求出现一个未知的错误 ERROR.M6M");
                        return openVo;
                    }else {
                        signService.save(new VefSign(null,currentHeader,new Date()));
                    }
                }else {
                    openVo.setState(false);
                    openVo.setMsg("重复提交请求，请稍后再试");
                    return openVo;
                }
            }
            return openVo;
        }catch (Exception e){
            openVo.setState(false);
            openVo.setMsg("请求异常 解析请求失败");
            return openVo;
        }
    }
}

package com.open.schedule;

import com.open.bean.ImageRecognition;
import com.open.bean.RequestRecord;
import com.open.service.ImageRecognitionService;
import com.open.service.RequestRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author typsusan
 * <p>des</p>
 **/
@Configuration
@Slf4j
public class ImageRecognitionTask {

    @Autowired
    ImageRecognitionService imageRecognitionService;

    @Autowired
    RequestRecordService requestRecordService;

    @Scheduled(cron = "0 0 23 * * ?")
    public void imageRecognition(){
        List<ImageRecognition> imageRecognitionList = imageRecognitionService.list();
        List<Integer> ids = new ArrayList<>();
        for (ImageRecognition recognition : imageRecognitionList) {
            List<RequestRecord> requestRecordList = requestRecordService.lambdaQuery()
                    .eq(RequestRecord::getGroupCode, recognition.getMsgCode())
                    .eq(RequestRecord::getIsDelete,1)
                    .list();
            // 说明删除了
            if (requestRecordList.size() != 0) {
                File file = new File(recognition.getImg());
                file.delete();
                ids.add(recognition.getRid());
            }
        }
        imageRecognitionService.removeByIds(ids);
    }
}

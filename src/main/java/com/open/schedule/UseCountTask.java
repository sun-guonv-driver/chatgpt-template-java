package com.open.schedule;

import com.open.bean.IpWhite;
import com.open.bean.RequestToken;
import com.open.service.IpWhiteService;
import com.open.service.RequestRecordService;
import com.open.service.RequestTokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;
import java.util.Map;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@Configuration
@Slf4j
public class UseCountTask {

    @Autowired
    RequestTokenService requestTokenService;

    @Autowired
    IpWhiteService ipWhiteService;

    @Scheduled(cron = "0 0 1 * * ?")
    private void morning() {
        // 更新使用次数
        /**
         * 1, 用户标识2 plus1用户 每天赠送35次访问重置次数 3   10元套餐
         * 2，用户标识3 plus2用户 每天赠送65次访问 重置次数 7   20 元套餐
         * 3，用户标识4 plus3用户 每天赠送95次访问 重置次数12   30 元套餐(格外赠送两次重置机会)
         *
         *  重置会累加前一天的额度
         */
        List<IpWhite> whiteList = ipWhiteService.lambdaQuery()
                .ge(IpWhite::getUserType, 2)
                .gt(IpWhite::getResettingCount,0)
                .eq(IpWhite::getIpStatus, 0).list();
        for (IpWhite white : whiteList) {
            if (white.getResettingCount() != 0){
                if (white.getUserType() == 2){
                    ipWhiteService.lambdaUpdate()
                            .set(IpWhite::getRequestCount,white.getRequestCount()+35)
                            .eq(IpWhite::getUserType,2)
                            .eq(IpWhite::getWid,white.getWid()).update();
                }else if (white.getUserType() == 3){
                    ipWhiteService.lambdaUpdate()
                            .set(IpWhite::getRequestCount,white.getRequestCount()+65)
                            .eq(IpWhite::getUserType,3)
                            .eq(IpWhite::getWid,white.getWid()).update();
                }else if (white.getUserType() == 4){
                    ipWhiteService.lambdaUpdate()
                            .set(IpWhite::getRequestCount,white.getRequestCount()+95)
                            .eq(IpWhite::getUserType,4)
                            .eq(IpWhite::getWid,white.getWid()).update();
                }
                ipWhiteService.lambdaUpdate()
                        .set(IpWhite::getResettingCount,white.getResettingCount()-1)
                        .eq(IpWhite::getWid,white.getWid()).update();
            }
        }
    }

}
